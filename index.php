<!DOCTYPE html>

<html>

<head>
    <title>Project no. 9</title>
    <link rel="stylesheet" type="text/css" href="css/style.css"> 
    <link rel="icon" href="images/favicon.jpg">
</head>

<body>  
    <div class="wrapper">
        <header class="header">
            <img  class="header__logo-image" src="images/logo.jpg" alt="Logo image" title="Logo image">            
        </header>        
        <!-- End of header -->
        <div class="main long">
            <?php
            require('inc/config.php');
            
            echo '<h1>List of films in database</h1>';
            
            $sql = "SELECT film.id, film.title, genre.genre, film.runtime_minutes, film.summary,
                dateofrelease.date FROM film
                INNER JOIN genre ON film.genre_id = genre.id
                INNER JOIN dateofrelease ON film.release_id = dateofrelease.id;";
            $result = mysqli_query($connection, $sql) or die(mysqli_error($connection));            

            if (mysqli_num_rows($result) != 0) {
                
                echo '<table class="table">
                    <tr>
                        <th>No.</th>
                        <th>Title</th>
                        <th>Genre</th>
                        <th>Runtime</th>
                        <th>Summary</th>
                        <th>Date of Release</th>
                    </tr>';
                
                // $counter instead of id because id sequence won't always be correct
                $counter = 1;
                while ($row = mysqli_fetch_assoc($result)) {
                   // echo $row["id"]." ".$row["title"]." ".$row["genre"]." ".$row["runtime_minutes"]." ".$row["summary"]." ".$row["date"] .";                    
                    $filmid = $row["id"];                    
                    echo "<tr>                    
                            <td>".$counter."</td>
                            <td>".$row["title"]."</td>
                            <td>".$row["genre"]."</td>
                            <td>".$row["runtime_minutes"]."</td>
                            <td>".$row["summary"]."</td>
                            <td>".$row["date"]."</td>
                            <td><a href=\"update.php?filmid=$filmid\">Edit</a></td>
                        </tr>";
                    $counter++;
                }
                echo "</table><br>";
                echo "There are " . --$counter . " films listed.<br>";

                mysqli_free_result($result);
            }
            mysqli_close($connection);            
            ?>
            
        </div>
    </div> 

</body>

</html>