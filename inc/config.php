<?php
define("HOST", "localhost");
define("USER", "user1");
define("PASSWORD","pass123");
define("DATABASE","moviedb");

$connection = mysqli_connect(HOST, USER, PASSWORD, DATABASE) or die(mysqli_connect_error());

if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}
?>