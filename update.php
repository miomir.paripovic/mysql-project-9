<?php
require('inc/config.php');

if (isset($_GET['filmid'])) { $filmid = $_GET['filmid']; }

//$sql = "SELECT * FROM film WHERE id = $filmid";
//$result = mysqli_query($connection, $sql) or die(mysqli_error($connection));

$sql = "SELECT film.id, film.title, genre.genre, film.runtime_minutes, film.summary,
    dateofrelease.date FROM film
    INNER JOIN genre ON film.genre_id = genre.id
    INNER JOIN dateofrelease ON film.release_id = dateofrelease.id
    WHERE film.id = $filmid";
//echo $sql;
$result = mysqli_query($connection, $sql) or die(mysqli_error($connection));            

// test
//$row = mysqli_fetch_assoc($result);
//var_dump($row);

if (mysqli_num_rows($result) != 0) {
    while($row = mysqli_fetch_assoc($result)) {
        //var_dump($row);
        $itemid = $row["id"];
        $itemtitle = $row["title"];
        $itemgenreid = $row["genre"];
        $itemruntime = $row["runtime_minutes"];
        $itemsummary = $row["summary"];
        $itemrelease = $row["date"];
    }
    //echo $itemtitle . "<br>";
    //echo $itemruntime;
    mysqli_free_result($result);
}
mysqli_close($connection);
?>

<!DOCTYPE HTML>  

<html>

<head>
    <title>Project no. 9</title>
    <link rel="stylesheet" type="text/css" href="css/style.css"> 
    <link rel="icon" href="images/favicon.jpg">
</head>

<body>  
    <div class="wrapper">
        <header class="header">
            <img  class="header__logo-image" src="images/logo.jpg" alt="Logo image" title="Logo image">            
        </header>        
        <!-- End of header -->
        <div class="main">            
            <form action="realupdate.php" method="POST">
                <fieldset class="fieldset">
                    <legend class="legend">You are to update film:</legend>
                    <label>Title</label><br>
                    <input type="text" name="title" class="std-input" value="<?php echo $itemtitle; ?>"><br><br>
                    <label>Genre (not editable)</label><br>
                    <input type="text" name="genre" class="std-input" value="<?php echo $itemgenreid; ?>" readonly><br><br>
                    <label>Time</label><br>
                    <input type="text" name="runtime_minutes" class="std-input" value="<?php echo $itemruntime; ?>"><br><br>
                    <label>Summary</label><br>
                    <textarea name="summary" class="std-input" rows="12" cols="37"><?php echo $itemsummary; ?></textarea><br><br>
                    <label>Release Date (not editable)</label><br>
                    <input type="text" class="std-input" name="release_id" value="<?php echo $itemrelease; ?>" readonly><br><br>    
                    <input type="hidden" name="itemid" value="<?php echo $itemid; ?>"><br>                    
                    <input type="submit" name="submitbutton" id="submitbutton" value="update">
                    <?php
                    echo "<a href=\"index.php\">Cancel edit</a><br>";
                    ?>
                </fieldset>
            </form>
        </div>
    </div> 

</body>

</html>