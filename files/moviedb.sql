-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 06, 2018 at 05:37 PM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `moviedb`
--

-- --------------------------------------------------------

--
-- Table structure for table `dateofrelease`
--

DROP TABLE IF EXISTS `dateofrelease`;
CREATE TABLE IF NOT EXISTS `dateofrelease` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dateofrelease`
--

INSERT INTO `dateofrelease` (`id`, `date`) VALUES
(1, '2009-12-25'),
(2, '1982-06-25'),
(3, '2004-09-26'),
(4, '1986-07-18'),
(5, '1980-06-20'),
(6, '1969-07-04'),
(7, '1986-07-02'),
(8, '1985-08-23'),
(9, '2005-04-01'),
(10, '1999-03-31');

-- --------------------------------------------------------

--
-- Table structure for table `film`
--

DROP TABLE IF EXISTS `film`;
CREATE TABLE IF NOT EXISTS `film` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `genre_id` int(11) NOT NULL,
  `runtime_minutes` int(3) NOT NULL,
  `summary` text NOT NULL,
  `release_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `film_ibfk_1` (`release_id`),
  KEY `genre_id` (`genre_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `film`
--

INSERT INTO `film` (`id`, `title`, `genre_id`, `runtime_minutes`, `summary`, `release_id`) VALUES
(1, 'Sherlock Holmes', 2, 128, 'After finally catching serial killer and occult \"sorcerer\" Lord Blackwood, legendary sleuth Sherlock Holmes and his assistant Dr. Watson can close yet another successful case. But when Blackwood mysteriously returns from the grave and resumes his killing spree, Holmes must take up the hunt once again. ', 1),
(2, 'Blade Runner', 7, 117, 'A blade runner must pursue and try to terminate four replicants who stole a ship in space and have returned to Earth to find their creator. ', 2),
(3, 'Eulogy', 3, 91, 'A black comedy that follows three generations of a family, who come together for the funeral of the patriarch - unveiling a litany of family secrets and covert relationships. ', 3),
(4, 'Aliens', 7, 137, 'Ellen Ripley is rescued by a deep salvage team after being hypersleep for 57 years. The moon that the Nostromo visited has been colonized, but contact is lost. This time, colonial marines have impressive firepower, but will that be enough? ', 4),
(5, 'Star Wars: Episode V - The Empire Strikes Back', 7, 124, 'After the rebels are brutally overpowered by the Empire on the ice planet Hoth, Luke Skywalker begins Jedi training with Yoda, while his friends are pursued by Darth Vader. ', 5),
(6, 'Once Upon a Time in the West', 10, 164, 'A mysterious stranger with a harmonica joins forces with a notorious desperado to protect a beautiful widow from a ruthless assassin working for the railroad. ', 6),
(7, 'Big Trouble in Little China', 2, 99, 'An All-American trucker gets dragged into a centuries-old mystical battle in Chinatown. ', 7),
(8, 'Godzilla', 7, 103, 'Thirty years after the original monster\'s rampage, a new Godzilla emerges and attacks Japan. ', 8),
(9, 'Sin City', 8, 124, 'A film that explores the dark and miserable town, Basin City, and tells the story of three different people, all caught up in violent corruption. ', 9),
(10, 'The Matrix', 7, 136, 'A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers. ', 10);

-- --------------------------------------------------------

--
-- Table structure for table `genre`
--

DROP TABLE IF EXISTS `genre`;
CREATE TABLE IF NOT EXISTS `genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `genre` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `genre`
--

INSERT INTO `genre` (`id`, `genre`) VALUES
(1, 'Action'),
(2, 'Adventure'),
(3, 'Comedy'),
(4, 'Crime'),
(5, 'Drama'),
(6, 'Horror'),
(7, 'Sci-Fi'),
(8, 'Thriller'),
(9, 'War'),
(10, 'Western');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `film`
--
ALTER TABLE `film`
  ADD CONSTRAINT `film_ibfk_1` FOREIGN KEY (`release_id`) REFERENCES `dateofrelease` (`id`),
  ADD CONSTRAINT `film_ibfk_2` FOREIGN KEY (`genre_id`) REFERENCES `genre` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
